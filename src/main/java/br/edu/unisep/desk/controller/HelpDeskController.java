package br.edu.unisep.desk.controller;

import br.edu.unisep.desk.domain.dto.helpdesk.HelpDeskDto;
import br.edu.unisep.desk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.desk.domain.usecase.helpdesk.FindAllHelpDeskUseCase;
import br.edu.unisep.desk.domain.usecase.helpdesk.FindResponsavelHelpDesk;
import br.edu.unisep.desk.domain.usecase.helpdesk.FindUsuarioHelpDesk;
import br.edu.unisep.desk.domain.usecase.helpdesk.RegeisterHelpDeskUseCase;
import br.edu.unisep.desk.response.DefaltResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/HelpDesk")
public class HelpDeskController {

    private final FindAllHelpDeskUseCase findAllHelpDeskUseCase;
    private final FindUsuarioHelpDesk findUsuarioHelpDesk;
    private final RegeisterHelpDeskUseCase regeisterHelpDeskUseCase;
    private final FindResponsavelHelpDesk findResponsavelHelpDesk;

    @GetMapping
    public ResponseEntity<List<HelpDeskDto>> findAll() {
        var desks = findAllHelpDeskUseCase.execute();

        return desks.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(desks);
    }

    @GetMapping("/byUsername")
    public ResponseEntity<DefaltResponse<List<HelpDeskDto>>> findUserHelpDesk(@RequestParam("title") String username) {
        var desk = findUsuarioHelpDesk.execute(username);

        return desk.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaltResponse.of(desk));
    }

    @PostMapping
    public ResponseEntity<DefaltResponse<Boolean>> save(@RequestBody RegisterHelpDeskDto desk) {
        regeisterHelpDeskUseCase.execute(desk);
        return ResponseEntity.ok(DefaltResponse.of(true));
    }

    @GetMapping("/byResponsavel")
    public ResponseEntity<DefaltResponse<List<HelpDeskDto>>> findResponsavelHelpDesk(@RequestParam("responsavel") String nameResponder) {
        var responder =findResponsavelHelpDesk.execute(nameResponder);

        return responder.isEmpty() ?
            ResponseEntity.noContent().build() :
            ResponseEntity.ok(DefaltResponse.of(responder));
    }

}
