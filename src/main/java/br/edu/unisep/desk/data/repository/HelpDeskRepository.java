package br.edu.unisep.desk.data.repository;


import br.edu.unisep.desk.data.entity.HelpDesk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelpDeskRepository extends JpaRepository<HelpDesk, Integer> {
    List<HelpDesk> findByUserContainingIgnoreCase(String username);

    List<HelpDesk> findByResponseContainingIgnoreCase(String nameResponder);


}
