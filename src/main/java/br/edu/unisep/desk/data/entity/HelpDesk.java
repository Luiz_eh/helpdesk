package br.edu.unisep.desk.data.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tickets")
public class HelpDesk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Integer id;

    @Column(name = "name_issuer")
    private String username;

    @Column(name = "email_issuer")
    private String email;

    @Column(name = "title")
    private String title;

    @Column(name = "name_responder")
    private String nameResponder;

    @Column(name = "status")
    private Integer status;

    @Column(name = "open_date")
    private Date cadastro;

    @Column(name = "close_date")
    private Date fechamento;

    @Column(name = "email_responder")
    private String emailResponder;

    @Column(name = "description")
    private  String descricao;
}