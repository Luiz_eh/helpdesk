package br.edu.unisep.desk.domain.builder.helpdesk;

import br.edu.unisep.desk.data.entity.HelpDesk;
import br.edu.unisep.desk.domain.dto.helpdesk.HelpDeskDto;
import br.edu.unisep.desk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.desk.domain.dto.helpdesk.UpdateHelpDeskDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component

public class HelpDeskBuilder {

    public List<HelpDeskDto> from(List<HelpDesk> helpDesks){
        return helpDesks.stream().map(this::from).collect(Collectors.toList());
    }

    public HelpDeskDto from(HelpDesk helpDesks) {
        return new HelpDeskDto(
                helpDesks.getId(),
                helpDesks.getUsername(),
                helpDesks.getEmail(),
                helpDesks.getTitle(),
                helpDesks.getNameResponder(),
                helpDesks.getStatus(),
                helpDesks.getCadastro(),
                helpDesks.getFechamento(),
                helpDesks.getEmailResponder(),
                helpDesks.getDescricao()

        );
    }
    public HelpDesk from(UpdateHelpDeskDto updateHelpDeskDto) {
        HelpDesk desk = from((RegisterHelpDeskDto) updateHelpDeskDto);
        desk.setId(updateHelpDeskDto.getId());

        return desk;
    }


    public HelpDesk from(RegisterHelpDeskDto registerHelpDesk){
        HelpDesk desk = new HelpDesk();
        desk.setUsername(registerHelpDesk.getUsername());
        desk.setEmail(registerHelpDesk.getEmail());
        desk.setTitle(registerHelpDesk.getTitle());
        desk.setNameResponder(registerHelpDesk.getNameResponder());
        desk.setStatus(registerHelpDesk.getStatus());
        desk.setCadastro(registerHelpDesk.getCadastro());
        desk.setFechamento(registerHelpDesk.getFechamento());
        desk.setEmailResponder(registerHelpDesk.getEmailResponder());
        desk.setDescricao(registerHelpDesk.getDescricao());

        return desk;

    }
}
