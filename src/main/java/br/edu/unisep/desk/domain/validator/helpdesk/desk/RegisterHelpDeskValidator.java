package br.edu.unisep.desk.domain.validator.helpdesk.desk;

import br.edu.unisep.desk.domain.dto.helpdesk.RegisterHelpDeskDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;
import static br.edu.unisep.desk.domain.validator.helpdesk.ValidationMessages.*;
@Component
public class RegisterHelpDeskValidator {

    public void validate(RegisterHelpDeskDto registerHelpDeskDto) {
        Validate.notBlank(registerHelpDeskDto.getUsername(), MESSAGE_REQUIRED_DESK_USERNAME);
        Validate.notBlank(registerHelpDeskDto.getEmail(), MESSAGE_REQUIRED_DESK_EMAIL);
        Validate.notBlank(registerHelpDeskDto.getEmailResponder(), MESSAGE_REQUIRED_DESK_EMAILRESPONDER);
        Validate.notBlank(registerHelpDeskDto.getDescricao(), MESSAGE_REQUIRED_DESK_DESCRICAO);
        Validate.notBlank(registerHelpDeskDto.getTitle(), MESSAGE_REQUIRED_DESK_TITLE);
        Validate.notBlank(registerHelpDeskDto.getDescricao(),MESSAGE_REQUIRED_DESK_RESPONDER);
    }
}
