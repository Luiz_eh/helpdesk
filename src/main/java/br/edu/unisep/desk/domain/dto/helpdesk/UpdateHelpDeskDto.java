package br.edu.unisep.desk.domain.dto.helpdesk;

import lombok.Data;

@Data
public class UpdateHelpDeskDto extends RegisterHelpDeskDto {

    private Integer id;
}
