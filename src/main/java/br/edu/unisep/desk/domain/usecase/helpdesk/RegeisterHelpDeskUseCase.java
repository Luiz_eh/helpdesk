package br.edu.unisep.desk.domain.usecase.helpdesk;

import br.edu.unisep.desk.data.repository.HelpDeskRepository;
import br.edu.unisep.desk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.desk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.desk.domain.validator.helpdesk.desk.RegisterHelpDeskValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegeisterHelpDeskUseCase {

    private final RegisterHelpDeskValidator validator;
    private final HelpDeskBuilder builder;
    private final HelpDeskRepository helpDeskRepository;

    public void execute(RegisterHelpDeskDto registerHelpDesk) {
        validator.validate(registerHelpDesk);

        var desk = builder.from(registerHelpDesk);
        helpDeskRepository.save(desk);

    }
}